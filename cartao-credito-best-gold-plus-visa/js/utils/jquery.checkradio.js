// ***************************************** 
// Author: 		Luis Freire
// Date:		2013-09-20
// Description: Check Radio Plugin   
// *****************************************

(function($, window, document, undefined) {
	var pluginName = 'checkradio',
		$this = null,
		self = this;
		
	var CheckRadio = function( element, options ) {
        this.element = $(element);
        this._name = pluginName + parseInt(Math.random() * 999999999);
        this._type = $(element).data("type");
		$this = this;
		this.init(options);
    }
    
    
	
	// Init plugin
	CheckRadio.prototype = {
        
		init: function(options) {
			this.options = options;
			if (this.element.hasClass("checkradio-processed")) return;
			this.element.find("label").each(function() {
				if ($("input", this).attr("checked")) $(this).addClass("checked");
			}).bind("click", $this.select);
			this.element.addClass("checkradio-processed");
		},
		
		select: function(event) {
			
			if ($(this).hasClass("disabled")) return;
			
			var cr = $(this).parents(".checkradio");
			
			if ($(event.target).hasClass("morelink") || $(event.target).is("a")) return;
			
			event.preventDefault();
			
			if ($this.options.beforeChange) $this.options.beforeChange.apply(this, [input]);
			var input = $("input", this);
			var type = $("input", this).attr("type");
			
			if (type == "checkbox") {
				$(this).toggleClass("checked");
			} else { // Radio
				$("input", cr).attr("checked", false);
				$("label", cr).not(this).removeClass("checked");
				$(this).addClass("checked");
			}
			$("input", this).attr("checked", $(this).hasClass("checked") ? true : false);
			if ($this.options.afterChange) $this.options.afterChange.apply(this, [input]);
			input.trigger("change");
			$this.error(false);
			
			return this;
		},
		
		
		value: function(val) {
		
			var results = [];
			var result = null;
			
			
			if (typeof(val) == "undefined") {
				
				// return a value
				this.element.find("label").each(function() {
					var type = $("input", this).attr("type");
					if (type == "checkbox") {
						if ($(this).hasClass("checked")) results.push( $("input", this).val() );
					} else {
						whichtype = "radio";
						if ($(this).hasClass("checked")) result = $("input", this).val();
					}
				});
				
				return this._type == "radio" ? result : (results.length > 0 ? results : null);
			
			} else {
				
				// Select option
				this.element.find("label").each(function() {
					var input = $("input", this);
					var cr = $(this).parents(".checkradio");
					var type = $("input", this).attr("type");
					
					if (input.val() == val) {
						if (type == "checkbox") {
							$(this).toggleClass("checked");
						} else { // Radio
							$("input", cr).attr("checked", false);
							$("label", cr).not(this).removeClass("checked");
							$(this).addClass("checked");
						}
						$("input", this).attr("checked", $(this).hasClass("checked") ? true : false);
					}
					
					
					
				});
				
			}
			
		},
		
		clear: function() {
			$this = this;
			this.element.find("label").each(function() {
				$("input", $this.element).attr("checked", false);
				$("label", $this.element).removeClass("checked");
			});
		},
		
		enable: function() {
			$this = this;
			this.element.find("label").each(function() {
				$(this).removeClass("disabled");
			});
		},
		
		disable: function() {
			$this = this;
			this.element.find("label").each(function() {
				$(this).addClass("disabled");
			});
		},
		
		
		length: function() {
			return this.element.find("label").length;
		},
		
		
		error: function(value) {
		
			if (typeof(value) == "undefined") {
				return this.element.hasClass("error");
			} else {
				if (value == true) {
					this.element.addClass("error");
				} else {
					this.element.removeClass("error");
				}
			}
			
			return $(this);
			
		}
		
		
    };
	
	
	$.fn[pluginName] = function ( option, val ) {
	
		if (typeof option == 'string') {
			switch(option) {
				case "length":
					return $("label", this).length;
					break;
				case "value":
					if (typeof(val) == "undefined") return $("input[checked='checked']", this).val();
					break;
			}
		}
		
		
		return this.each(function (index) {
			
			var $this = $(this),
				data = $this.data(pluginName),
				options = typeof option === 'object' && option;
			
			if (!data)  {
				$this.data(pluginName, (data = new CheckRadio(this, $.extend({}, $.fn[pluginName].defaults, options))));
			} 
			
			
			if (typeof(option) === 'string') {
				data[option](val);
			}
		})
	};

	
	$.fn[pluginName].defaults = {
		beforeChange: null,
		afterChange: null
	};

	$.fn[pluginName].Constructor = CheckRadio;
	
	
	$(document).ready(function() {
		$(".checkradio").checkradio();
		
	});
	
})(jQuery, window, document);