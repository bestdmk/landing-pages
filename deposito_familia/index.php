<?php

error_reporting(0);
//nao reportar erros
if ($_SERVER['HTTP_HOST'] != 'localhost' && $_SERVER['HTTP_HOST'] != 'dev.hiperformancesales.com') {
  if (!isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == "") {
    $redirect = "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    header("Location: $redirect");
  }
  date_default_timezone_set ('Europe/Lisbon');
  include("functions.php");
  require_once('Connections/fconn.php');

  $step = "s1";
  //Step definition
  $idCampanha = 1;
  //da id da camapnha da tabela campaigns
  $query = $fconn->query("SELECT * FROM campaigns WHERE id = '$idCampanha' ");
  $numero = mysqli_num_rows($query);
  //se a campanha existe na BD
  if ($numero == 1) {
    $query = $fconn->query("SELECT * FROM campaigns WHERE id = '$idCampanha' ");
    $resultado = mysqli_fetch_array($query);
    $campanha = $resultado['campname'];
  }

  if (isset($_GET["p1"])) {
    $partner1id = $_GET["p1"];
    //Lead Track QueryString Partner 1 Code
  }
  //Lead Track QueryString Partner 1 Code
  if (isset($_GET["p2"])) {

    $partner2id = $_GET["p2"];
    //Lead Track QueryString Partner 2 Code
  }
  //Lead Track QueryString Partner 2 Code

  if (isset($_GET["cpid"])) {
    $campaignid = $_GET["cpid"];
    //Lead Track QueryString Campaign Code
  }
  if (isset($_GET["tid"])) {
    $typeid = $_GET["tid"];
    //Lead Track QueryString Campaign Type Code
  }
  $regdate = date("Y-m-d G:i:s");
  //Today Date and Time
  $ip = getRealIpAddr();
  //IP address
  if (!empty($_SERVER['HTTP_REFERER'])) {
    $httpref = $_SERVER['HTTP_REFERER'];
    //HTTP Referer
  }

  $insertSQL = sprintf("INSERT INTO leadin (regdate, prt1code, prt2code, campname, ctypecode, ip, httpref, nav, step) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)", GetSQLValueString($regdate, "date", $fconn), GetSQLValueString($partner1id, "text" , $fconn), GetSQLValueString($partner2id, "text" , $fconn), GetSQLValueString($campanha, "text" , $fconn), GetSQLValueString($typeid, "text", $fconn), GetSQLValueString($ip, "text", $fconn), GetSQLValueString($httpref, "text", $fconn), GetSQLValueString('h;', "text", $fconn), GetSQLValueString($step, "text", $fconn));

  //mysql_select_db($database_fconn, $fconn);

  $Result1 = $fconn->query($insertSQL);

  $last_in_id = mysqli_insert_id($fconn);


  ######## integracao BO ########

  require_once('Connections/backoffice.php');

  $campanha = 2;

  $insertSQL = sprintf("INSERT INTO leadin (regdate, prt1code, prt2code, campname, ctypecode, ip, httpref, step, campanha, nav) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", GetSQLValueString($regdate, "date", $backoffice), GetSQLValueString($partner1id, "text", $backoffice), GetSQLValueString($partner2id, "text", $backoffice), GetSQLValueString($campanha, "text", $backoffice), GetSQLValueString($typeid, "text", $backoffice), GetSQLValueString($ip, "text", $backoffice), GetSQLValueString($httpref, "text", $backoffice), GetSQLValueString($step, "text", $backoffice), GetSQLValueString('2', "text", $backoffice), GetSQLValueString('h;', "text", $backoffice));

 $backoffice->query($insertSQL);

  $BOlid = mysqli_insert_id($backoffice);

  if (!empty($_GET['kw'])) {

    $keyword = mysqli_real_escape_string($backoffice, $_GET['kw']);

    $backoffice->query("INSERT INTO keyword values (0, '$BOlid', '$keyword')");
  }

  if (!empty($_GET['ct'])) {

    $content = mysqli_real_escape_string($backoffice, $_GET['ct']);

    $backoffice->query("INSERT INTO content values (0, '$BOlid', '$content')");
  }
}
######## integracao BO ########
?>
<!DOCTYPE HTML>
<html lang="pt">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
  <title>Banco Best - Depósito a Prazo 2%</title>
  <meta name="dcterms.rightsHolder" content="" />
  <meta name="dcterms.rights" content="" />
  <meta name="dcterms.dateCopyrighted" content="2015" />
  <meta name="description" content="Depósito a Prazo Novos Clientes 2% tanb a 90 dias. Receba os juros no dia seguinte à constituição.Saiba mais." />
  <meta name="keywords" content="depósito a prazo, deposito a prazo, banco best, best bank, best deposito, deposito banco best, banco online." />
  <link rel="shortcut icon" href="https://www.bancobest.pt/favicon.ico" />
  <link rel="icon" type="image/ico" href="https://www.bancobest.pt/favicon.ico" />

  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700,900&display=swap" rel="stylesheet">

  <link rel="stylesheet" href="css/bootstrap.css">
  <link rel="stylesheet" href="css/owl.carousel.css">
  <link rel="stylesheet" href="css/style.css">

  <!--[if lt IE 9]>
        <script src="js/add-ins/html5.js"></script>
        <![endif]-->
  <!--INICIO TAG GOOGLE SEARCH CONSOLE -->
  <meta name="google-site-verification" content="vwKWeZjqMguHtk0IyNU0pZxJUdosOsXujn2-qaBz3lk" />
  <!--FIM TAG GOOGLE SEARCH CONSOLE -->
  <!-- Google Tag Manager -->
  <script>
    (function(w, d, s, l, i) {
      w[l] = w[l] || [];
      w[l].push({
        'gtm.start': new Date().getTime(),
        event: 'gtm.js'
      });
      var f = d.getElementsByTagName(s)[0],
        j = d.createElement(s),
        dl = l != 'dataLayer' ? '&l=' + l : '';
      j.async = true;
      j.src =
        'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
      f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-K9GN3MS');
  </script>
  <!-- End Google Tag Manager -->
</head>

<body style="display:none;">
  <!-- Google Tag Manager (noscript) -->
  <noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K9GN3MS" height="0" width="0" style="display:none;visibility:hidden"></iframe>
  </noscript>
  <!-- End Google Tag Manager (noscript) -->
  <!-- COOkIE header-->
  <div class="bb-header-container" style="margin: auto;">
    <div id="topboxcookies" class="hidden-phone boxcookies">
      <div class="boxcookies-content">
        <h4 style="font-size:12px; font-weight: bold; margin:8px 0 0;">POLÍTICA DE UTILIZAÇÃO DE COOKIES DO BANCO BEST</h4>
        <p style="margin-bottom:10px; font-size: 12px;">Este website utiliza cookies de acordo com a política em vigor. Ao continuar a navegação está a consentir expressamente
          na sua utilização.</p>
        <a style="font-size:10px; padding:4px 8px; border:1px solid #1c4f90; color:#1c4f90; border-radius:15px;" target="_blank" href="https://www.bancobest.pt/ptg/best_documentacao-legal">Saber mais</a>
        <a style="font-size:10px; padding:4px 8px; border:1px solid #1c4f90; color:#1c4f90; border-radius:15px" href="javascript:;" id="closetopboxcookies"> Aceitar</a>
      </div>
    </div>

  </div>

  <div class="overlay"></div>
  <div class="btn-mobile">
    <a href="javascript:;" class="btn-cta-mobile">Aderir</a>
  </div>

  <header>
    <div class="container">
      <div class="header-content">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-4">
            <img src="img/logo.png" class="header-logo">
          </div>
          <div class="col-md-6 col-sm-6 col-xs-8">
            <p class="header-note desktop">Ao lado de quem vai à frente</p>
          </div>
        </div>
        <div class="row">
        <div class="col-md-12">
            <div class="header-block clearfix">
              <div class="header-block-left">
                <div class="txt-block">
                  <p class="pre-title">Depósito a prazo Novos Clientes</p>
                  <h1>Não espere para ver o seu dinheiro crescer</h1>
                  <div class="offer">
                      <p class="percentage">2<span>%</span></p>
                      <p class="tax">TANB a<br>90 dias</p>
                  </div>
                  <ul>
                      <li>| Pagamento antecipado de juros</li>
                      <li>| Montantes entre 2.500€ e 40.000€</li>
                      <li>| Sem mobilização antecipada</li>
                      <li>| Limitado a um depósito por morada</li>
                  </ul>
                </div>

              </div>
              <div class="header-block-right">
                <div class="close-box">
                  <img src="img/icon-close.png">
                </div>
                <h2>Pedido de Contacto</h2>
                <p class="subtitle">Preencha e envie o seu pedido de contacto.</p>

                <div class="form-block">

                  <form action="step2.php" method="POST" id="mainForm">
                    <div class="form-group">
                      <label for="name">Nome e Apelido</label><p class="error">Insira um nome válido.</p>
                      <input class="form-field" id="name" name="name" type="text" value="" autocomplete="off" placeholder="Insira o seu nome">
                      
                    </div>
                    <div class="form-group">
                      <label for="email">E-mail</label><p class="error">Insira um email válido.</p>
                      <input class="form-field" id="email" name="email" type="text" value="" autocomplete="off" placeholder="Insira o seu Email">
                    </div>
                    <div class="form-group">
                      <label for="phone">Telefone</label><p class="error">Insira um telefone válido.</p>
                      <input class="form-field" id="phone" name="phone" type="text" value="" maxlength="14" autocomplete="off" placeholder="Insira o seu Telefone">
                    </div>
                    <div class="form-group">
                      <label for="nif">Número de Contribuinte</label><p class="error">Insira um Número válido.</p>
                      <input class="form-field" id="nif" name="nif" type="text" value="" autocomplete="off" maxlength="9" placeholder="Insira o seu Nº de Contribuinte">
                    </div>
                    <?php echo "<input type='hidden' name='lid' id='lid' value='" . $last_in_id . "'>"; ?> 
                    <?php echo "<input type='hidden' name='BOlid' id='BOlid' value='" . $BOlid . "'>"; ?>
                    <div class="form-group" id="termsBox">
                      <label class="container-checkbox">
                          Declaro que li e tomei conhecimento das <a href="https://www.bancobest.pt/ptg/bestsite/best_docs/Informacoes_Tratamento_Dados_Pessoais.pdf" target="_blank">Informações sobre o Tratamento de Dados Pessoais</a> do Banco Best. Autorizo o tratamento dos meus dados pessoais pelo BEST, neste âmbito e em futuras ações de marketing relevantes para mim.
                        <p class="error">Aceite para continuar.</p>
                        <input type="checkbox" id="terms">
                        <span class="checkmark"></span>
                        
                      </label>
                      
                    </div>
                    <div class="btn-block">
                      <a href="javascript:;" class="btn-cta" id="btn-submit">Enviar</a>
                    </div>
                  </form>

                </div>
              </div>
            </div>
          </div>
          
        </div>
      </div>
    </div>
    
  </header>

  <section class="juros">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2>Receba os juros logo no dia seguinte</h2>
          <p class="subtitle">Abra a sua primeira conta no Banco Best, subscreva o depósito a prazo e receba os juros logo no dia seguinte.</p>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="vantagens">
            <h3>Vantagens do Depósito a Prazo Novos Clientes</h3>
            <div class="vantagens-block">
              <div class="vantagens-txt-block">
                <p><span class="blue">2% TANB a 90 dias</span><br>para começar, uma taxa superior à média do mercado</p>
              </div>
              <div class="vantagens-txt-block">
                <p><span class="blue">Não tem que esperar</span><br>Recebe os juros no dia seguinte à constituição do depósito a prazo</p>
              </div>
              <div class="vantagens-txt-block">
                <p><span class="blue">Sem Risco</span><br>Depósito com garantia total de capital e juros</p>
              </div>
              <div class="vantagens-txt-block">
                <p><span class="blue">PROTEÇÃO</span><br>Depósito abrangido pelo Fundo de garantia de Depósitos</p>
              </div>
            </div>
            <div class="btn-block">
              <a href="javascript:;" class="btn-orange" id="aderirMiddle">Aderir</a>
            </div>

            <div class="footer-note">
              <p>Consulte a <a href="https://www.banco-best.pt/mlfiles/FIN_NovosClientes_2_00.pdf?_ga=2.134592113.1858082967.1568223083-1856168800.1567088191" target="_blank">Ficha de informação normalizada</a></p>
              <p>Campanha não acumulável com outras campanhas em vigor para novos Clientes.</p>
            </div>
          </div>
        </div>
      </div>
  </section> 

  <section class="porque">
    <div class="container">
      <h3>Porquê o Banco Best?</h3>
      <div class="row">
        <div class="owl-carousel">
          <div class="col-md-3">
            <div class="porque-txt">
              <h4>ACESSO FÁCIL</h4>
              <p>website, app Best Bank, Contact Centre, nos Centros de Investimento ou através do seu Gestor ou Consultor Externo.</p>
            </div>
          </div>
          <div class="col-md-3">
            <div class="porque-txt">
              <h4>0€ NAS TranSFERÊncias</h4>
              <p>a crédito intrabancárias e SEPA+, no website, app e ATM</p>
            </div>
          </div>
          <div class="col-md-3">
            <div class="porque-txt">
              <h4>0€ NA MANUTENÇÃO</h4>
              <p>da conta Digital em euros</p>
            </div>
          </div>
          <div class="col-md-3">
            <div class="porque-txt">
              <h4>INVESTIMENTOS</h4>
              <p>20€ é quanto precisa para começar a subscrever produtos de investimento</p>
            </div>
          </div>
        </div>
      </div>
      <div class="btn-block">
        <a href="javascript:;" class="btn-orange" id="abraConta">Abra já a sua conta</a>
      </div>
    </div>
  </section>

  <section class="faqs">
    <h3>FAQs</h3>
  <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="faqs-content">
            <div class="faq-line">
              <p class="question">Como abrir conta?</p>
              <p class="answer">
                  <span class="dark-blue bold">Pode abrir a sua conta Online:</span><br>
                  <strong>Chave Móvel Digital:</strong> A partir do seu smartphone ou computador, pode optar por abrir a sua conta com a Chave Móvel Digital, dando a devida autorização e seguindo as instruções. No final basta apenas que nos reencaminhe o email que recebeu com a confirmação da abertura de conta, anexando o comprovativo de profissão (consulte abaixo os comprovativos aceites) e envie para atualizacaodados@bancobest.pt. Caso tenha indicado ter outras residências fiscais deverá enviar-nos também o comprovativo do NIF Estrangeiro (emitido por entidade idónea).<br><br>
				  
				  <strong>Preenchendo manualmente:</strong> Pode optar por fazer uma videochamada* com o nosso assistente que vai ajudá-lo em todos os passos da abertura de conta. <br><br>
                  
                  Selecione <span class="dark-blue">Abrir Conta</span>, introduza os seus dados pessoais e escolha a opção <span class="dark-blue">Tratar já por videochamada</span>. Depois, escolha a opção smartphone ou computador, faça upload de uma fotografia do cartão de cidadão, comprovativo de morada e profissão e inicie a videochamada. Siga as instruções do assistente, assine os documentos de forma eletrónica e já está!<br><br>
                  
                  A autenticação é eletrónica e por SMS desde que o telemóvel utilizado seja validado durante a videochamada. O contrato assinado ficará válido quando o processo de videochamada e a assinatura por SMS forem concluídos com sucesso.<br><br>
        
                  *Videochamada disponível para portadores de Cartão de Cidadão, residentes em Portugal e para abertura de conta com um máximo de 2 titulares. <br><br>
				  A abertura de conta online só está disponível para maiores de 18 anos. <br><br>
                  
                  <!--<span class="dark-blue bold">Ou ainda, abrir a sua conta num Centro de Investimento:</span><br>
                  Diriga-se a um dos nossos Centros de Investimento, com o cartão de cidadão, comprovativo de morada e profissão e terá todo o apoio na abertura da sua conta.
				  <br>--><br>
                  
                  <span class="dark-blue bold">Documentos:</span><br>
                  	  <strong>Documento de identificação de todos os titulares válido:</strong> São considerados documentos de identificação: bilhete de identidade, cartão do cidadão, passaporte e boletim de nascimento, caso se trate de menores até aos 10 anos de idade (inclusive).<br><br>
					  <strong>Cartão de contribuinte de todos os titulares:</strong> Para os titulares que tenham residências fiscais fora de Portugal é também necessária cópia autenticada do cartão de identificação fiscal estrangeiro ou documento idóneo que o comprove.<br><br>
					  <strong>Comprovativo de morada (residência permanente e fiscal, se diferentes) de todos os titulares com data inferior a um ano:</strong> Poderá ser uma fatura da água, eletricidade ou outra.<br><br>
					  <strong>Comprovativo de profissão/entidade patronal de todos os titulares com data inferior a um ano:</strong> Poderá ser um recibo de vencimento ou uma declaração da entidade patronal. Não aplicável a domésticas nem a estudantes menores de idade. Para estudantes maiores de idade: cartão de aluno ou declaração do estabelecimento de ensino com data de emissão inferior a um ano. Para desempregados: comprovativo de inscrição no Centro de Emprego. Para reformados: cartão de pensionista ou declaração anual do último ano civil.<br><br>
					  <strong>Comprovativo da origem do património (apenas aplicável a Pessoas Politicamente Expostas – PEP):</strong> Poderá ser uma declaração do Tribunal Constitucional, declaração de IRS ou outro documento que ateste a origem do património.
              </p>
            </div>
            <div class="faq-line">
              <p class="question">Quanto Pago no Banco Best? 0€</p>
              <p class="answer">
                  0€ de manutenção de conta na Conta Digital em euros. As transferências a crédito intrabancárias e SEPA +, os pagamentos e os carregamentos também são gratuitos na internet, mobile e ATM. Consulte o <a href="https://www.bancobest.pt/ptg/best_NS_PRECARIO?_ga=2.137883696.1858082967.1568223083-1856168800.1567088191" class="link" target="_blank">preçário</a>.
              </p>
            </div>
            <div class="faq-line">
              <p class="question">Conheça o Banco best</p>
              <p class="answer">
                No Banco Best somos especialistas nas áreas de Banking, Asset Management e Trading e disponibilizamos produtos e serviços financeiros à escala global.<br>

                Para além de termos uma plataforma tecnológica de última geração para gerir o seu dia a dia, investir e negociar de forma rápida e simples, temos também uma oferta global e independente, uma vez que, trabalhamos com os melhores bancos, seguradoras e sociedades gestoras. Temos mais de 3.000 fundos de investimento nacionais e internacionais, bem como milhares de ativos financeiros para negociar em mais de 20 mercados no mundo.<br>

                Contamos também com uma vasta equipa de profissionais especializados em mercados  e ativos financeiros com elevada mobilidade e disponibilidade.<br>

               Tudo isto de simples e de fácil acesso através da web, App Best Bank (24 horas por dia e 365 dias do ano), do nosso Contact Centre, do seu Gestor ou Consultor Externo e nos Centros de Investimento.<br>
              </p>
            </div>
            <div class="faq-line">
              <p class="question">Centros de Investimento</p>
              <p class="answer">
                Pode consultar os Centros de Investimento do Banco Best mais perto de si <a href="https://www.bancobest.pt/ptg/best_redes-comerciais?_ga=2.137883696.1858082967.1568223083-1856168800.1567088191" target="_blank" class="link">aqui</a>.
              </p>
            </div>
            <div class="faq-line">
              <p class="question">E se tiver dúvidas?</p>
              <p class="answer">
                Entre em contacto connosco através do telefone 218 505 775 nos dias úteis das 8h às 22h. Se preferir, visite os nossos Centros de Investimento. Para mais informações aceda a www.bancobest.pt > Contactos.
              </p>
            </div>
          </div>

          
        </div>
      </div>
    </div>
  </section>


  <footer>    
    <div class="container">
      <div class="footer-content">
        <div class="row">
          <div class="col-md-6 col-sm-7">
            <div class="footer-content-left">
              <img src="img/logo.png" class="logo-footer">
              <p>© 2020 Banco Best. Todos os direitos reservados.</p>
            </div>
          </div>
          <div class="col-md-6 col-sm-5">
            <div class="footer-content-right">
              <img src="img/icon-phone.png">
              <p class="number">218 505 775</p>
              <p>DIAS ÚTEIS 8H-22H</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>

  <script type="text/javascript" src="//code.jquery.com/jquery-1.10.0.min.js"></script>
  <script src="js/owl.carousel.js"></script>
  <script src="js/main.js"></script>

  <!-- Google Analytics -->
  <script>
    (function(i, s, o, g, r, a, m) {
      i['GoogleAnalyticsObject'] = r;
      i[r] = i[r] || function() {
        (i[r].q = i[r].q || []).push(arguments)
      }, i[r].l = 1 * new Date();
      a = s.createElement(o),
        m = s.getElementsByTagName(o)[0];
      a.async = 1;
      a.src = g;
      m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-47314333-4', 'auto');
    ga('send', 'pageview');
  </script>
  <!-- Google Code for Remarketing tag -->
  <!-- Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. For instructions on adding this tag and more information on the above requirements, read the setup guide: google.com/ads/remarketingsetup -->
  <script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 972317687;
    var google_conversion_label = "LGnaCOn80gQQ98fRzwM";
    var google_custom_params = window.google_tag_params;
    var google_remarketing_only = true;
    /* ]]> */
  </script>
  <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>
  <noscript>
    <div style="display:inline;"> <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/972317687/?value=0&amp;label=LGnaCOn80gQQ98fRzwM&amp;guid=ON&amp;script=0" /> </div>
  </noscript>

</body>
</html>
