// ON DOM READY INIT APPLICATION.
$(document).ready(function () {
    //------Cookies Functions
    var cook = getCookie("BESTCookie");
    if (cook == "") {
        setTimeout(function () {

            $(".bb-header-container").prepend($("#topboxcookieshtml").html());
            $("#topboxcookieshtml").remove();
            $("#topboxcookies").slideDown("slow");
            $("#closetopboxcookies, #closebottomboxcookies").click(function () {
                setCookie("BESTCookie", "1;secure;", 365);
                $("#topboxcookies").slideUp("slow");
                $("#bottomboxcookies").animate({ "bottom": "-100px" }, 'slow');
                $("body.hasCookie").removeClass("hasCookie");
            });
            $("body.mobile").addClass("hasCookie");


        }, 3000);

    } else {
        $("#topboxcookies").remove();
    }

    $("body").css('display', 'none');
    setTimeout(function () {
        $("body").fadeIn();        
        // - - -
        var hash = window.location.hash;
        if (hash.length > 1) {
            var hPos = $(hash).offset().top;
            $("body,html").animate({ scrollTop: hPos }, '0');
        }
    }, 1500);
    $("body").append('<div class="d-modal"></div>');
    $(".d-modal").load("agreed-modal.html");
});

$('#modal-agreed .btn-read').on('click', function () {

    if ($('.checkradio label').hasClass("checked")) {
        which.submit();
    }

});
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = $.trim(ca[i]);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toGMTString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}


// * * *
// * CODE PSALES
// * * *
$( document ).ready(function() {
    //desktop CTAs
    $('.btn-cta').click(function(){
        $(".header-block").fadeIn('fast', function(){
            var t = $(".header-block").offset().top;
            $("body, html").animate({"scrollTop": (t-20)},500)
        });
        
    });
    $('.link').click(function(){
        $(".header-block").fadeIn('fast', function(){
            var t = $(".header-block").offset().top;
            $("body, html").animate({"scrollTop": (t-20)},500)
        });
    });

    //mobile CTA 
    $('.btn-cta-mobile').click(function(){
        $('.overlay').show();
        $('.header-block-right').show();
    });

    $('.close-box img').click(function(){
        $('.overlay').hide();
        $('.header-block-right').hide();
    });

    //carousel
    if (window.innerWidth < 991) {
        $('.porque .owl-carousel').owlCarousel({
            loop:false,
            nav:false,
            dots:true,
            items: 1
        })
    }

    //faqs
    $('.question').click(function() {
        $(this).parent().find('.answer').toggle();
        $(this).toggleClass('open');
    });


    $('.form-field').focus(function(){
        $(this).parent().find('.error').hide();
    });


    // send form
    $('#btn-submit').click(function(){

        var name = $('#name').val();
        var email = $('#email').val();
        var phone = $('#phone').val();
        var nif = $('#nif').val();

        //VALIDA NOME
        var nameCheck = name.split(' ');
        if ($.trim(name).length == 0 || $.trim(name).length < 3) {
            $('#name').parent().find('.error').css('display','inline-block');

        } else if (name.indexOf(' ') == '-1') {
            $('#name').parent().find('.error').css('display','inline-block');

        } else if (nameCheck[1] == '') {
            $('#name').parent().find('.error').css('display','inline-block');

        } else if (nameCheck[1].length <= 1 || nameCheck[0].length <= 1 || typeof nameCheck[0] === 'undefined') {
            $('#name').parent().find('.error').css('display','inline-block');
        }

        //VALIDA MAIL
        if ($.trim(email).length == 0) {
            $('#email').parent().find('.error').css('display','inline-block');         
        } 

        if (!validateEmail(email)) {
            $('#email').parent().find('.error').css('display','inline-block');           
        } 

        //VALIDA TEL
        if ($.trim(phone).length < 9) {
            $('#phone').parent().find('.error').css('display','inline-block');
        } 

        if (!validatePhone(phone)) {
            $('#phone').parent().find('.error').css('display','inline-block');
        } 

        //VALIDA NIF
        if(nif.length < 9 || !validateNif(nif)) {
            $('#nif').parent().find('.error').css('display','inline-block');
        }


        //VALIDA CHECKBOX
        if ($('#terms').is(':checked')) {
            $(".hasError").removeClass('hasError');
            $('#terms').parents().eq(1).find('.error').hide();
        }
        else {
            $("#termsBox").addClass("hasError");
            $('#terms').parents().eq(1).find('.error').css('display','inline-block');
        }


        if( $(".error:visible").length <= 0 ){
            $("#mainForm").submit();
        }

    });


});


function validateEmail(email) {
    var reg = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/

    if (reg.test(email)){
        return true;
    }
    else {
        return false;
    }
} 

function validatePhone(phone) {
   var filter = /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/;   
   if (filter.test(parseInt(phone))) {
       return true;
   }
   else {
       return false;
   }
}

function validateNif(nif) {
    var re = (/^([0-9]{9})$/);
    return re.test(nif);
}